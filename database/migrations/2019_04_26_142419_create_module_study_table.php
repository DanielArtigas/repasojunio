<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleStudyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_study', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')->references('id')->on('modules')
            ->onDelete('cascade');
            $table->integer('study_id')->unsigned();
            $table->foreign('study_id')->references('id')->on('studies')
            ->onDelete('cascade');
            $table->integer('course')->unsigned()->nullable();
            $table->primary(['module_id', 'study_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_study');
    }
}
