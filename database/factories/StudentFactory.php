<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname'=> $faker->lastName,
        'date' => $faker->date,
        'direccion' => $faker->address,
        'email' => $faker->unique()->safeEmail


        
    ];
});
