<?php

use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('studies')->insert([
            'code' => 'IFC303',
            'name' => 'Desarrollo de Aplicaciones web',
            'family_id'=>'1',
        ]);
        DB::table('studies')->insert([
            'code' => 'ADG301',
            'name' => 'Administración y Finanzas',
            'family_id'=>'2',
        ]);
        DB::table('studies')->insert([
            'code' => 'IMP301',
            'name' => 'Asesoría de Imagen Personal',
            'family_id'=>'3',
        ]);
    }
}

