<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        DB::table('families')->insert([
            'code' => 'IFC',
            'name' => 'Informatica y comunicaciones',
        ]);

         DB::table('families')->insert([
            'code' => 'IMP',
            'name' => 'Imagen Personal',
        ]);

          DB::table('families')->insert([
            'code' => 'ADG',
            'name' => 'Administración y gestión',
        ]);

    }
}

