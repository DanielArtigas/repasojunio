<?php

use Illuminate\Database\Seeder;

class Module_StudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $study = \App\Study::find(1);
        $study->modules()->attach(1, ['course' => 1]);
        $study->modules()->attach(2, ['course' => 1]);
        $study->modules()->attach(3, ['course' => 1]);
    }
}
