<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(ExamsTableSeeder::class);
        $this->call(StudentSeeder::class);
        $this->call(FamiliesTableSeeder::class);
        $this->call(StudiesTableSeeder::class);
        $this->call(Module_StudyTableSeeder::class);

        
    }
}
