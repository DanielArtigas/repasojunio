@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Modulos</h1>
            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>

                    </tr>
                </thead>

                <tbody>
                    @foreach($module as $module )

                        <tr>
                            <td>{{$module->name}}</td>
                            @can('view', $module)
                             <td><a  href="/modules/<?php echo $module->id ?>" class="btn btn-primary"  role="button" >Ver</a></td>
                            @else
                            <td>no veas</td>
                            @endcan
                            <td><a  href="/modules/<?php echo $module->id ?>/edit" class="btn btn-primary"  role="button" >Edit</a></td>
                            <td>
                            <form method="post" action="/modules/{{$module->id}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
                            </form>
                            </td>


                         </tr>

                    @endforeach
                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection