@extends('layouts.app')

@section('content')
 <div class="container">
  <div class="row justify-content-center">
   <div class="col-md-12">
    <h1>Create de Modulos</h1>
     <form class="form"  method="post" action="/modules">
      {{ csrf_field() }}

       <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="{{old('name')}}">

         @if ($errors->first('name'))
          <div class="alert alert-danger ">
           {{$errors->first('name')}}
          </div>
         @endif

       </div>

       <div class="form-group">
        <label>Codigo</label>
        <input class="form-control" type="text" name="{{old('code')}}">

         @if ($errors->first('code'))
          <div class="alert alert-danger ">
           {{$errors->first('code')}}
          </div>
         @endif

       </div>



        <input type="submit" value="Nuevo modulo" class="btn btn-primary"  role="button">
        <a href="/modules" class="btn btn-primary"  role="button">Volver</a>
     </form>
   </div>
  </div>
 </div>
@endsection