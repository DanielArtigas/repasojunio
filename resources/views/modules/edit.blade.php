@section('content')
  <div class="container">
   <div class="row justify-content-center">
    <div class="col-md-12">
     <h1>Edit de Module</h1>
      <form class="form"  method="post" action="/modules/{{$module->id}}">
       {{ csrf_field() }}

        <input type="hidden" name="_method" value="put">

         <div class="form-group">
          <label>Nombre</label>
          <input class="form-control" type="text" name="name"
          value="{{$module->name}}">

          @if ($errors->first('name'))
         <div class="alert alert-danger ">
          {{$errors->first('name')}}
         </div>
          @endif


         </div>

         <div class="form-group">
          <label>Codigo</label>
          <input class="form-control" type="text" name="code"
          value="{{$module->code}}">

          @if ($errors->first('code'))
         <div class="alert alert-danger ">
          {{$errors->first('code')}}
         </div>
          @endif
         </div>

      <input type="submit" value="Nuevo" class="btn btn-primary"  role="button">
      <a href="/modules" class="btn btn-primary"  role="button">Volver</a>
     </form>
    </div>
   </div>
  </div>
@endsection