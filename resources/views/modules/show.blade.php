@extends('layouts.app')

@section('title', 'Module')

@section('content')
            <h1>
               Lista de Modulos <?php echo $module->id ?>
            </h1>

            <ul>
                <li>Nombre: {{$module->name}} </li>
            </ul>

             <h2>
              Lista de Exams <?php echo $module->id ?>
             </h2>

             <ul>
                @foreach($module->exams as $module )
                <li> Exam: {{$module->title}}</li>
                @endforeach
             </ul>


            <a href="/modules" class="btn btn-primary"  role="button">Volver</a>


@endsection