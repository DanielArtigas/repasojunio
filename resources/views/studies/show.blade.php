@extends('layouts.app')

@section('title', 'Studies')

@section('content')
            <h1>Lista de Estudios <?php echo $study->id ?></h1>

            <ul>
                <li>Nombre: {{$study->name}} </li>
                <li>Codigo: {{$study->code}}</li>
                <li>Familia: {{$study->family->name}}</li>
                <li>Modulo:
                    <ul>
                        @foreach ($study->modules as $module)
                            <li> Curso: {{ $module->pivot->course }} - {{ $module->name }}
                            <form method="post" action="/studies/{{$study->id}}/modules">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="module_id" value="{{$module->id}}">
                                <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
                            </form>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
    <h2>Formulario de Curso y Modulo</h2>
        <ul>
            <form class="form"  method="post" action="/studies/{{$study->id}}/modules">
                {{ csrf_field() }}

            <div class="form-group">
                <label>Curso</label>
                <input class="form-control" type="text" name="course"
          value="{{$module->pivot->course}}">
                @if ($errors->first('course'))
                <div class="alert alert-danger ">
                {{$errors->first('course')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Modulos</label>
                <select class="form-control" type="text" name="module_id" >
                    <option></option>
                    @foreach($modules as $module)
                    <option value="{{$module->id}}">{{$module->name}}</option>
                    @endforeach
                </select>
                <input class="form-control" type="text" name="{{old('module')}}">
                @if ($errors->first('module'))
                <div class="alert alert-danger ">
                {{$errors->first('module')}}
                </div>
                @endif
            </div>

            <input type="submit" value="Añadir" class="btn btn-primary"  role="button">

            </form>
        </ul>
    <a href="/studies" class="btn btn-primary"  role="button">Volver</a>
@endsection
