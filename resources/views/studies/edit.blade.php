@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Editar de Estudios</h1>
            <form class="form"  method="post" action="/studies/{{$study->id}}">
                {{ csrf_field() }}

                 <input type="hidden" name="_method" value="put">

                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name"
                    value="{{$study->name}}">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Codigo</label>
                    <input class="form-control" type="text" name="code"
                     value="{{$study->code}}">


                    @if ($errors->first('code'))
                    <div class="alert alert-danger ">
                        {{$errors->first('code')}}
                    </div>
                    @endif

                </div>

                 <div class="form-group">
                    <label>Familia</label>
                    <select class="form-control" type="text" name="family_id">
                        <option></option>
                        @foreach($family as $family)
                        <option value="{{$family->id}}"{{old('family_id') == $family->id ? 'selected="selected"' : ' '}}>{{$family->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->first('family_id'))
                    <div class="alert alert-danger ">
                     {{$errors->first('family_id')}}
                 </div>
                 @endif
             </div>

                <input type="submit" value="Editar Estudios" class="btn btn-primary"  role="button">

                <a href="/studies" class="btn btn-primary"  role="button">Volver</a>
            </form>
        </div>

    </div>
</div>
@endsection