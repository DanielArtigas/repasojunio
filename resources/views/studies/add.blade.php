@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <h1>Lista del create de Modulos y Curso</h1>
          <form class="form"  method="post" action="/studies/{{$study->id}}">
            {{ csrf_field() }}
              <div class="form-group">
                <label>Curso</label>
                  <input class="form-control" type="text" name="course"  value="{{$study->course}}">
              </div>

              <div class="form-group">
                <label>Modulos</label>
                  <select class="form-control" type="text" name="module_id" >
                    <option></option>
                      @foreach($study->modules as $module)
                        <option value="{{$module->id}}" {{$module->module_id == $module->id ? 'selected="selected"' : ' '}}>{{$module->name}}</option>
                      @endforeach
                                        </select>
              </div>

              <input type="submit" value="Añadir" class="btn btn-primary"  role="button">
              <a href="/studies/{{$study->id}}" class="btn btn-primary"  role="button">Volver</a>
          </form>
      </div>
    </div>
  </div>
@endsection
