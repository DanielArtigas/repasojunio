@extends('layouts.app')

@section('title', 'Question')

@section('content')
            <h1>
              Lista de Questions <?php echo $question->id ?>
            </h1>

            <ul>
                <li>Question: {{$question->text}} </li>
                <li>Module: {{$question->module->name}} </li>

            </ul>

            <h2>
              Lista Exams <?php echo $question->id ?>
            </h2>

             <ul>
                @foreach($question->exams as $question )
                <li> Exam: {{$question->title}}</li>
                @endforeach
             </ul>


            <a href="/questions" class="btn btn-primary"  role="button">Volver</a>


@endsection