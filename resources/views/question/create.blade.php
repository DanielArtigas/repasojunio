@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>Alta de Preguntas</h1>
                    <form class="form"  method="post" action="/questions">
                    {{ csrf_field() }}

                        <div class="form-group">
                            <label>Pregunta</label>
                            <input class="form-control" type="text" name="{{old('text')}}">

                            @if ($errors->first('text'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('text')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>A</label>
                            <input class="form-control" type="text" name="{{old('a')}}">

                            @if ($errors->first('a'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('a')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>B</label>
                            <input class="form-control" type="text" name="{{old('b')}}">

                            @if ($errors->first('b'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('b')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>C</label>
                            <input class="form-control" type="text" name="{{old('c')}}">

                            @if ($errors->first('c'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('c')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>D</label>
                            <input class="form-control" type="text" name="{{old('d')}}">

                            @if ($errors->first('d'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('d')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Respuesta</label>
                            <input class="form-control" type="text" name="{{old('answer')}}">

                            @if ($errors->first('answer'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('answer')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Modulos</label>
                            <select class="form-control" type="text" name="module_id">
                            <option></option>
                                @foreach($module as $module)
                                    <option value="{{$module->id}}"{{old('module_id') == $module->id ? 'selected="selected"' : ' '}}>{{$module->name}}</option>
                                @endforeach
                            </select>

                            @if ($errors->first('module_id'))
                                <div class="alert alert-danger ">
                                    {{$errors->first('module_id')}}
                                </div>
                            @endif
                        </div>

                    <input type="submit" value="Nueva Pregunta" class="btn btn-primary"  role="button">
                    <a href="/questions" class="btn btn-primary"  role="button">Volver</a>
                </form>
            </div>
        </div>
    </div>
@endsection
