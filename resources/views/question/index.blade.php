@extends('layouts.app')

@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de preguntas</h1>
            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Pregunta</th>
                        <th>A</th>
                        <th>B</th>
                        <th>C</th>
                        <th>D</th>
                        <th>Respuesta</th>
                        <th>Modulo</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($question as $question )

                        <tr>
                             <td>{{$question->text}}</td>
                             <td class="
                                @if($question->answer == 'a')
                                    bg-success
                                @endif">
                            {{$question->a}}
                            </td>
                             <td class="
                                @if($question->answer == 'b')
                                    bg-success
                                @endif">
                                {{$question->b}}
                             </td>
                             <td class="
                                @if($question->answer == 'c')
                                    bg-success
                                @endif">
                                {{$question->c}}
                             </td>
                             <td class="
                                @if($question->answer == 'd')
                                    bg-success
                                @endif">
                             {{$question->d}}
                             </td>
                             <td>{{$question->answer}}</td>
                             <td>{{$question->module->name}}</td>
                             <td><a  href="/questions/{{$question->id}}" class="btn btn-primary"  role="button">Ver</a></td>
                             <td><a  href="/questions/{{$question->id}}/edit" class="btn btn-primary"  role="button">Edit</a></td>
                             <td>
                                <form method="post" action="/questions/{{$question->id}}">
                                  {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
                                </form>
                             </td>


                         </tr>
                    @endforeach
                     <a  href="/questions/create" class="btn btn-primary"  role="button">Nueva Pregunta</a>
                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection
