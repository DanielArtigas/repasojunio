@extends('layouts.app')

@section('title', 'Students')

@section('content')
            <h1>
               Lista de Estudiantes<?php echo $student->id ?>
            </h1>

            <ul>
                <li>Nombre: {{$student->name}} </li>
                <li>Apellido: {{$student->surname}}</li>
                <li>Fecha de Nacimiento: {{$student->date}}</li>
                <li>Direccion: {{$student->address}} </li>
                <li>Email: {{$student->email}}</li>

            </ul>

            <a href="/students" class="btn btn-primary"  role="button">Volver</a>

@endsection