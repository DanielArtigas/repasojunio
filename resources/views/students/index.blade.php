@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Students</h1>
        <table  class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Fecha de Nacimiento</th>
              <th>Direccion</th>
              <th>Email</th>
            </tr>
          </thead>

          <tbody>
            @foreach($student as $student )

              <tr>
                <td>{{$student->name}}</td>
                <td>{{$student->surname}}</td>
                <td>{{$student->date}}</td>
                <td>{{$student->address}}</td>
                <td>{{$student->email}} </td>
                <td><a href="/students/<?php echo $student->id ?>" class="btn btn-primary"  role="button" >Ver</a></td>
                <td><a href="/students/<?php echo $student->id ?>/edit" class="btn btn-primary"  role="button" >Editar</a></td>
                <td>
                  <form method="post" action="/students/{{$student->id}}">
                    {{ csrf_field() }}
                      <input type="hidden" name="_method" value="delete">
                      <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
                  </form>
                </td>
              </tr>

            @endforeach
            <a href="/students/create" class="btn btn-primary"  role="button" >Nuevo</a>

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection