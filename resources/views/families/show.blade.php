@extends('layouts.app')

@section('title', 'Families')

@section('content')
            <h1>Lista de Familias <?php echo $family->id ?></h1>

            <ul>
                <li>Nombre: {{$family->name}} </li>
                <li>Codigo: {{$family->code}}</li>
            </ul>

             <h2>
              Lista de estudios <?php echo $family->id ?>
             </h2>

             <ul>
                @foreach($family->studies as $study )
                <li> Estudios: {{$study->name}}</li>
                @endforeach
             </ul>


            <a href="/families" class="btn btn-primary"  role="button">Volver</a>


@endsection
