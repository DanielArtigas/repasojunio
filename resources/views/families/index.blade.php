@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Familias</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Codigo</th>
          </tr>
        </thead>

        <tbody>
          @foreach($families as $family )

          <tr>
           <td>{{$family->name}}</td>
           <td>{{$family->code}}</td>
           <td><a  href="/families/<?php echo $family->id ?>" class="btn btn-primary"  role="button" >Ver</a></td>
           <td><a  href="/families/<?php echo $family->id ?>/edit" class="btn btn-primary"  role="button" >Editar</a></td>
           <td>
            <form method="post" action="/families/{{$family->id}}">
             {{ csrf_field() }}
             <input type="hidden" name="_method" value="delete">
             <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
           </form>
         </td>
       </tr>

       @endforeach
       <a  href="/families/create" class="btn btn-primary"  role="button" >Nueva</a>
     </tbody>
   </table>

 </div>
</div>
</div>
@endsection