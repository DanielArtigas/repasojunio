@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Create de Familias</h1>
            <form class="form"  method="post" action="/families">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif


                </div>

                <div class="form-group">
                    <label>Codigo</label>
                    <input class="form-control" type="text" name="code">


                    @if ($errors->first('code'))
                    <div class="alert alert-danger ">
                        {{$errors->first('code')}}
                    </div>
                    @endif

                </div>



                <input type="submit" value="Nueva familia" class="btn btn-primary"  role="button">

                <a href="/families" class="btn btn-primary"  role="button">Volver</a>
            </form>
        </div>

    </div>
</div>
@endsection