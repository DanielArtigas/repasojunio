@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">

      <h1>Alta de examen</h1>

      <form class="form" method="post" action="/exams/new">
        {{ csrf_field() }}

        <div  class="form-group">
          <label>Título</label>
          <input  class="form-control"  type="text" name="title">
        </div>

        <div class="form-group">
          <label>Fecha</label>
          <input  class="form-control"  type="date" name="date">
        </div>

        <div class="form-group">
          <label>Módulo</label>
          <select  class="form-control"  name="module_id">
            <option></option>
            @foreach ($modules as $module)
            <option value="{{ $module->id}}"
              {{ old('module_id') == $module->id ? 'selected="selected"' : '' }}>
              {{ $module->name }}</option>
            @endforeach
          </select>
          @if ($errors->first('module_id'))
          <div class="alert alert-danger">
            {{$errors->first('module_id')}}
          </div>
          @endif
        </div>


        <div class="form-group">
            <label></label>
            <input class="form-control btn btn-primary"  type="submit" name="" value="Siguiente">
        </div>
      </form>
    </div>
  </div>
</div>
@endsection