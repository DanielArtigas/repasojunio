@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">

      <h1>Lista de examenes</h1>
      @if (Session::has('exam'))
      Examen recordado: {{ Session::get('exam')->title }}

      <a class="btn btn-warning" href="/exams/forget">Olvidarlo</a>
      @endif
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Title
            </th>
            <th>
              Date
            </th>
            <th>
              Materia
            </th>
            <th>
              Acciones
            </th>
          </tr>
        </thead>
        <tbody>

          @foreach($exams as $exam)
          <tr>
            <td>
              {{ $exam->title }}
            </td>
            <td>
              {{ $exam->date }}
            </td>
            <td>
              {{ $exam->module->name }}
            </td>

            <td>
              {{ $exam->user->name }}
            </td>

            <td>
              <form action="/exams/{{ $exam->id }}" method="post">
                <a  href="/exams/{{ $exam->id }}/edit" class="btn btn-primary"  role="button">Editar</a>
                <a  href="/exams/{{ $exam->id }}" class="btn btn-primary"  role="button">Ver</a>
                <a  href="/exams/{{ $exam->id }}/remember" class="btn btn-primary"  role="button">Recordar</a>
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-danger" type="submit" value="borrar">
              </form>
              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <a  href="/exams/new" class="btn btn-primary"  role="button">Nuevo Examen</a>
      <a  href="/exams/create" class="btn btn-primary"  role="button">Nuevo</a>
    </div>
  </div>
</div>
@endsection