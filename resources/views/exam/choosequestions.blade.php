@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row content">
      <div class="col-sm-12 text-left">
        <h1>Lista de Preguntas del Examen</h1>
        <h4>Módulo: {{ $module->name }}</h4>
        <h4>Título: {{ Session::get('title') }}</h4>
        <h4>Fecha:
        {{ date('d-m-Y', strtotime(Session::get('date'))) }}
        </h4>
        <ul>
        @if (Session::has('questions'))
         <h4>Preguntas añadidas:</h4> @foreach(Session::get('questions') as $question)
          <li>{{ $question->text }} 
          <a class="btn btn-danger" href="/exams/questions/{{ $question->id }}">Quitar</a>
          </li>
        @endforeach
        </ul>
        @endif
          <a class="btn btn-danger" href="/exams/reset">Reiniciar Alta</a>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Modulo</th>
              <th>Pregunta</th>
              <th>A</th>
              <th>B</th>
              <th>C</th>
              <th>D</th>
              <th>Respuesta</th>
            </tr>
          </thead>

          <tbody>
            @foreach($questions as $question)
              <tr>
                <td>{{$question->module->name}}</td>
                <td>{{$question->text}}</td>
                <td class=" @if($question->answer == 'a')
                  bg-success
                  @endif">
                    {{$question->a}}
                </td>
                <td class=" @if($question->answer == 'b')
                  bg-success
                  @endif">
                    {{$question->b}}
                </td>
                <td class=" @if($question->answer == 'c')
                  bg-success
                  @endif">
                    {{$question->c}}
                </td>
                <td class=" @if($question->answer == 'd')
                  bg-success
                  @endif">
                    {{$question->d}}
                </td>
                <td>{{$question->answer}}</td>
                <td>
              @if (isset(Session::get('questions')[$question->id]))
                <a class="btn btn-danger" href="/exams/questions/{{ $question->id }}">Quitar
                </a>
              @else
                <a class="btn btn-primary" href="/exams/questions/{{ $question->id }}">Añadir
                </a>
              @endif
            </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <a class="btn btn-primary" href="/exams/save">Crear Examen</a>
      </div>
    </div>
  </div>
@endsection