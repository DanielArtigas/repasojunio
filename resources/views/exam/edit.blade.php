@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">

      <h1>Edición de examen</h1>

      <form class="form" method="post" action="/exams/{{ $exam->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">

        <div  class="form-group">
          <label>Título</label>
          <input  class="form-control"  type="text" name="title" value="{{ $exam->title }}">
        </div>


        <div  class="form-group">
          <label>Fecha</label>
          <input  class="form-control"  type="date" name="date" value="{{ $exam->date}}">
        </div>


        <div  class="form-group">
          <label>Módulo</label>
          <select  class="form-control"  name="module_id">
            <option></option>
            @foreach ($module as $module)
            <option value="{{ $module->id}}"
              {{ $exam->module_id == $module->id ? 'selected="selected"' : '' }}>
              {{ $module->name }}
            </option>
            @endforeach
          </select>
        </div>

        <div  class="form-group">
          <label>User</label>
          <select  class="form-control"  name="user_id">
            <option></option>
            @foreach ($user as $user)
            <option value="{{ $user->id}}"
              {{ $exam->user_id == $user->id ? 'selected="selected"' : '' }}>
              {{ $user->name }}
            </option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
            <label></label>
            <input class="form-control btn btn-primary"  type="submit" name="" value="Nuevo">
        </div>



      </form>
    </div>
  </div>
</div>
@endsection