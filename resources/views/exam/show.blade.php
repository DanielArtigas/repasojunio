@extends('layouts.app')

@section('title', 'Exams')

@section('content')
            <h1>
               Lista de Examenes <?php echo $exam->id ?>
            </h1>

            <ul>
                <li>Titulo: {{$exam->title}} </li>
                <li>Fecha: {{$exam->date}}</li>

            </ul>

            <h2>
              Lista de Preguntas <?php echo $exam->id ?>
            </h2>
              <ul>
                @foreach($exam->questions as $exam)
                <li> Question {{$exam->text}}</li>
                @endforeach
              </ul>

            <a href="/exams" class="btn btn-primary"  role="button">Volver</a>
            


@endsection