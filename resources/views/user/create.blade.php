@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row justify-content-center">
  <div class="col-md-12">
   <h1>Create de User</h1>
    <form class="form"  method="post" action="/users">
     {{ csrf_field() }}

      <div class="form-group">
       <label>Nombre</label>
        <input class="form-control" type="text" name="name">
      </div>

      <div class="form-group">
       <label>Email</label>
        <input class="form-control" type="text" name="email">
      </div>

       <input type="submit" value="Nuevo" class="btn btn-primary"  role="button">
        <a href="/users" class="btn btn-primary"  role="button">Volver</a>
    </form>
  </div>
 </div>
</div>
@endsection