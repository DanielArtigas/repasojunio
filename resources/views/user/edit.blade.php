@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row justify-content-center">
  <div class="col-md-12">
   <h1>Edit de User</h1>
    <form class="form"  method="post" action="/users/{{$user->id}}">
     {{ csrf_field() }}

      <input type="hidden" name="_method" value="put">

       <div class="form-group">
        <label>Nombre</label>
         <input class="form-control" type="text" name="name"
          value="{{$user->name}}">
       </div>

       <div class="form-group">
        <label>Email</label>
         <input class="form-control" type="text" name="email"
          value="{{$user->email}}">
       </div>

        <input type="submit" value="Nuevo" class="btn btn-primary"  role="button">

        <a href="/users" class="btn btn-primary"  role="button">Volver</a>
    </form>
  </div>
 </div>
</div>
@endsection