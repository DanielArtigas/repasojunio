@extends('layouts.app')

@section('title', 'Users')

@section('content')
            <h1>Lista de Usuarios</h1>

            <ul>
                <li>Name: {{$user->name}} </li>
                <li>Email: {{$user->email}}</li>
            </ul>
            <a href="/users" class="btn btn-primary"  role="button">Volver</a>
@endsection