<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable =['code','name'];
    
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }

    public function studies()
    {
        return $this->belongsToMany(Study::class)->withPivot('course');
    }
}
