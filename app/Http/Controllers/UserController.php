<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{

    public function index()
    {
        $user=User::all();
        return view('user.index', ['user'=>$user]);
    }

    public function create()
    {
        $user=User::all();
        return view('user.create', ['user'=>$user]);
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->fill($request->all());
        $user->save();
        return redirect('/users');
    }

    public function show($id)
    {
        $user=User::find($id);
        return view('user.show', ['user'=>$user]);
    }

    public function edit($id)
    {
        $user=User::find($id);
        return view('user.edit', ['user'=>$user]);
    }

    public function update(Request $request, $id)
    {
        $user=User::find($id); 
        $user->fill($request->all());
        $user->save();
        return redirect('/users');
    }

    public function destroy($id)
    {
        User::destroy($id);
        return back();
    }
}