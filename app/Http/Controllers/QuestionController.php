<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;

class QuestionController extends Controller
{

    public function index()
    {
        $question=Question::all();
        return view('question.index',['question'=>$question]);
    }

    public function create()
    {
        $question=Question::all();
        $module= Module::all();
        return view('question.create',['question'=>$question],['module'=>$module]);
    }

    public function store(Request $request)
    {
        $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
        ];

        $request->validate($rules);

        $question = new Question;
        $question->fill($request->all());
        $question->save();
        return redirect('/questions');
    }

    public function show($id)
    {
        $question=Question::find($id);
        return view('question.show',['question'=>$question]);
    }

    public function edit($id)
    {
        $question=Question::find($id);
        $module= Module::all();
        return view('question.edit',['question'=>$question,'module'=>$module]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
        ];

        $request->validate($rules);

        $question=Question::find($id); 
        $question->fill($request->all());
        $question->save();
        return redirect('/questions');
    }

    public function destroy($id)
    {
        Question::destroy($id);
        return back();
    }
}
