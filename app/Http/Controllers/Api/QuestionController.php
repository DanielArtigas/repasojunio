<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Exam;
use App\Question;
use App\Module;

class QuestionController extends Controller
{

    public function index()
    {
          return Question::with('exams','module')->get();
    }

    public function store(Request $request)
    {
        $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $question= new Question;
        $question->fill($request->all());
        $question->save();
  
        return $question;
    }

    public function show($id)
    {
       $question =  Question::with('text','a','b','c','d','answer','module_id')->find($id);
        if($question){
            return $question;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);

       }


        $question =  Question::find($id);
        if($question){

            $question->fill($request->all());
            $question->save();
            $question->refresh();
            return $question;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }

    }

    public function destroy($id)
    {
        Question::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
