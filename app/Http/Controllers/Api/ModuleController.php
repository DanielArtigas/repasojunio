<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Exam;
use App\Module;
use App\Question;

class ModuleController extends Controller
{

    public function index()
    {
         return Module::with('exams','questions')->get();
    }

    public function store(Request $request)
    {

        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
      }

      $module = new Module;
      $module->fill($request->all());
      $module->save();

      return $module;
    }

    public function show($id)
    {
        $module =  Module::with('code','name')->find($id);
        if($module){
            return $module;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
      }
        $module =  Module::find($id);

        if($module){
           $module->fill($request->all());
           $module->save();
           $module->refresh();
             return $module;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    public function destroy($id)
    {
        Module::destroy($id);
        return response()->json([
                'message'=>'Deleted',
        ],201);
    }
}
