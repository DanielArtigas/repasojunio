<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Exam;
use App\Module;

class ExamController extends Controller
{

    public function index()
    {
        //return Exam::all();
        return Exam::with('user')->get();
    }
  
    
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            //return $validator->errors();

            return response()->json($validator->errors(), 400);
        }

        $exam = new Exam;
        $exam-> fill($request->all());
        $exam->save();
        return $exam;
    }

    
    public function show($id)
    {
       $exam = Exam::with('user', 'module', 'questions')->find($id);
       if($exam){
           return $exam;
       } else{
           return response()->json(['message' => 'Record not found'],404);
       }
    }

        
    public function update(Request $request, $id)
    {
        
        $rules = [
            'title' => 'required|max:255',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            
            return response()->json($validator->errors(), 400);

        }

        $exam = Exam::with('user', 'module')->find($id);

        if (!$exam) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }

        $exam->fill($request->all());
        $exam->save();
        $exam->refresh();
        return $exam;
    }

    
    public function destroy($id)
    {
        Exam::destroy($id);
        return response()->json([
                'message'=>'Deleted',
        ],201);
    }
}
