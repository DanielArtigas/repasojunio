<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    public function index()
    {
        $families=Family::all();
        return view('families.index',['families'=>$families]);
    }

    public function create()
    {
        $families=Family::all();
        return view('families.create',['families'=>$families]);
    }

    public function store(Request $request)
    {
        $rules=[
            'code' => 'unique:families,code|required|max:255' ,
            'name' =>'unique:families,name|required|max:255',
            ];

        $request->validate($rules);

        $family = new Family;
        $family->fill($request->all());
        $family->save();
        return redirect('/families');
    }

    public function show($id)
    {
        $family=Family::find($id);
        return view('families.show',['family'=>$family]);
    }

    public function edit($id)
    {
        $family=Family::find($id);
        return view('families.edit',['family'=>$family]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'code' => 'unique:families,code|required|max:255' ,
            'name' =>'unique:families,name|required|max:255',
       ];

       $request->validate($rules);

       $family = Family::find($id);
       $family->fill($request->all());
       $family->save();
       return redirect('/families');
    }

    public function destroy($id)
    {
        Family::destroy($id);
        return back();
    }
}
