<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Study;
use App\Family;
use App\Module;
class StudyController extends Controller
{
    public function index()
    {
        $study=Study::all();
        return view('studies.index',['study'=>$study]);
    }

    public function create()
    {
        $study=Study::all();
        $family=Family::all();
        $modules=Module::all();
        return view('studies.create',['study'=>$study,'family'=>$family, 'modules'=>$modules]);
    }

    public function store(Request $request)
    {
         $rules=[
             'code' => 'unique:studies,code|required|max:255' ,
             'name' =>'unique:studies,name|required|max:255',
          ];

         $request->validate($rules);

         $study = new Study;
         $study->fill($request->all());
         $study->save();
         return redirect('/studies');
    }

    public function show($id)
    {
        $study=Study::find($id);
        $modules=Module::all();
        return view('studies.show',['study'=>$study, 'modules'=>$modules]);

    }

    public function edit($id)
    {
        $study=Study::find($id);
        $family=Family::all();
        $modules=Module::all();
        return view('studies.edit',['study'=>$study,'family'=>$family, 'modules'=>$modules]);
    }

    public function update(Request $request, $id)
    {
         $rules=[
             'code' => 'unique:studies,code|required|max:255' ,
             'name' =>'unique:studies,name|required|max:255',
          ];

         $request->validate($rules);

         $study = Study::find($id);
         $study->fill($request->all());
         $study->save();
         return redirect('/studies');
    }

    public function destroy($id)
    {
        Study::destroy($id);
        return back();
    }

    public function attachModule(Request $request,$id)
    {
        $rules=[
            'course' => 'unique:module,course|required|max:2' ,
            'module' =>'unique:module,module|required|max:255',
         ];
        $request->validate($rules);
        $study=Study::find($id);
        $study->fill($request->all());
        $study->save();
        $module_id=$request->input('module_id');
        $course=$request->input('course');
        $study->modules()->syncWithoutDetaching([$module_id =>['course'=>$course]]);
        return back();

    }

    public function detachModule(Request $request, $id)
    {
     $study=Study::find($id);
     $module_id=$request->input('module_id');
     $study->modules()->detach($module_id);
     return back();
 }
}
