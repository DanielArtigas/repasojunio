<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        $student= Student::all();
        return view('student.index', ['student'=>$student]);
    }

    public function create()
    {
        $student=Student::all();
        return view('student.create', ['student'=>$student]);
    }

    public function store(Request $request)
    {

        $rules=[
            'name' => 'unique:students,name|required|max:255' ,
            'surname' =>'unique:students,surname|required|max:255',
            'date'=>'unique:students,date|required|date',
            'direccion'=>'unique:students,address|required|max:255',
            'email'=>'unique:students,email|required|max:255',
        ];

        $request->validate($rules);

        $student= new Student;
        $student->fill($request->all());
        $student->save();

        return redirect('/students');
    }

    public function show($id)
    {
        $student=Student::find($id);
        return view('student.show', ['student'=>$student]);

    }

    public function edit($id)
    {
        $student=Student::find($id);
        return view('student.edit', ['student'=>$student]);
    }

    public function update(Request $request, $id)
    {
         $rules=[
            'name' => 'unique:students,name|required|max:255' ,
            'surname' =>'unique:students,surname|required|max:255',
            'date'=>'unique:students,date|required|date',
            'direccion'=>'unique:students,address|required|max:255',
            'email'=>'unique:students,email|required|max:255',
        ];

        $request->validate($rules);

        $student= new Student;
        $student->fill($request->all());
        $student->save();

        return redirect('/students');
    }

    public function destroy($id)
    {
        Student::destroy($id);
        return back();
    }

}