<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Module;
use App\User;
use App\Question;
use Session;

class ExamController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $exams = Exam::all();

        return view('exam.index', ['exams'=>$exams]);
    }

    
    public function create()
    {
        
        $module=Module::all();
        $user=User::all();
        return view('exam.create',['module'=>$module,'user'=>$user]);
    }

    
    public function store(Request $request)
    {
        $rules=[
            'title' => 'unique:exams,title|required|max:255' . $id,
            'module_id' => 'unique:exams,module_id|exists:modules,id' ,
            //'user_id' =>' exists:users,id',
            'date' => 'unique:exams,date|required|date',
        ];

        $request->validate($rules);

        $exam = new Exam;
        $exam->fill($request->all());
        $exam->user_id = \Auth::user()->id; 
        $exam->save();

        return redirect('/exams');

    }

    
    public function show($id)
    {
        $exam=Exam::find($id);
        return view('exam.show', ['exam'=>$exam]);
    }

    
    public function edit($id)
    {
        $exam= Exam::find($id);
        $user = User::all();
        $module= Module::all();
        return view('exam.edit',['exam'=>$exam, 'module'=>$module,'user'=>$user]);
    }

    
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255|unique:exams,title,' .$id,
            'module_id' => 'exists:modules,id',
            //'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];
        $request->validate($rules);

        $exam = Exam::findOrFail($id);
        $exam-> fill($request->all());
        $exam->save();
        return redirect('/exams/$id');
    }

    
    public function destroy($id)
    {
        $exam = Exam::find($id);

        $this->authorize('delete', $exam);
        
        $exam->delete();
        return back();
    }

    public function remember($id, Request $request)
    {
        $exam = Exam::find($id);
        $request->session()->put('exam', $exam);
        return back();
    }

    public function new(Request $request)
    {
        if(Session::has('module')){
            $modules=Session::get('module');
            $questions=Question::where('module_id',$modules->id)->get();
            return view('exam.choosequestions', ['module'=>$modules,'questions'=>$questions]);    
        }else{
            $modules=Module::all();
            return view('exam.new', ['modules'=>$modules]);
        }
    }

    public function choosequestions(Request $request,$id){
        $module=Module::find($id);
        $questions=Question::all();
        return view('exams.choosequestions',['module'=>$module,'questions'=>$questions]);
    }

    public function newSetModule(Request $request)
    {
        $module = Module::find($request->module_id);
        Session::put('module', $module);
        Session::put('title', $request->title);
        Session::put('date', $request->date);
        return redirect('/exams/new');
    }

    public function reset()
    {
        Session::forget('module');
        Session::forget('title');
        Session::forget('date');
        Session::forget('questions');
        return redirect('/exams/new');
    }

    public function switchQuestion($id)
    {
        $question = Question::find($id);
        $questions = Session::get('questions');
        if (isset($questions[$question->id])) {
            unset($questions[$id]);
        } else {
            $questions[$id] = $question;
        }

        Session::put('questions', $questions);
        return back();
    }

    public function save()
    {
        $exam = new Exam;
        $exam->user_id = \Auth::user()->id;
        $module = Session::get('module');
        $exam->module_id = $module->id;
        $exam->title = Session::get('title');
        $exam->date = Session::get('date');
        $exam->save();
        foreach (Session::get('questions') as $question) {
            $exam->questions()->attach($question->id);
        }

        Session::forget('module');
        Session::forget('title');
        Session::forget('date');
        Session::forget('questions');
        return redirect('/exams/' . $exam->id);
    }
    public function forget(Request $request)
    {
        $request->session()->forget('exam');
        return back();
    }
}
