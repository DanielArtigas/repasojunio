<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth;
use App\Module;
class ModuleController extends Controller
{

    public function index()
    {
        //$this->authorize('index', Module::class);
        // $user= \Auth::user();
        // if($user->can('index', Module::class)){
        //     return "Si puede!!!!!!";
        // }else{
        //     return "No puede!!!!!! :-((";
        // }
        $this->authorize('index', Module::class);
        $module= Module::all();
        return view('modules.index', ['module'=>$module]);
    }

    public function create()
    {
        $module= Module::all();

        return view('modules.create', ['module'=>$module]);
    }

    public function store(Request $request)
    {
        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $request->validate($rules);


        $module = new Module;
        $module->fill($request->all());
        $module->save();
        return redirect('/modules');
    }

    public function show($id)
    {
        $module = Module::find($id);
        $this->authorize('view', $module);
        return view('modules.show', ['module'=>$module]);
    }

    public function edit($id)
    {
        $module=Module::find($id);
        return view('modules.edit', ['module'=>$module]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $request->validate($rules);


        $module=Module::find($id); 
        $module->fill($request->all());
        $module->save();
        return redirect('/modules');
    }

    public function destroy($id)
    {
        Module::destroy($id);
        return back(); 
    }
}