<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['title', 'module_id', 'user_id', 'date'];

    function user()
    {
        return $this->belongsTo(User::class); 
    }

    function module()
    {
        return $this->belongsTo(Module::class);
    }

    function questions()
    {
        return $this->belongsToMany(Question::class)->withPivot('order');
    }
}
