<?php

namespace App\Policies;

use App\User;
use App\Exam;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Exam $exam)
    {
        //
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Exam $exam)
    {
        //
    }

    public function delete(User $user, Exam $exam)
    {
        //return true;
        return $user->id == $exam->user_id;
    }
}
