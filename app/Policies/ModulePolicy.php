<?php

namespace App\Policies;

use App\User;
use App\Module;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return true;
    }
    public function view(User $user, Module $module)
    {
        // return true;
        return !($module->id % 2);
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Module $module)
    {
        //
    }

    public function delete(User $user, Module $module)
    {
        //
    }
}
