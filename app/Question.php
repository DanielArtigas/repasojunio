<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable =['text','a','b','c','d','answer','module_id']; //coge la informacion del formulario y si esta mete la informacion sino no.

    public function exams()
    {
        return $this->belongsToMany(Exam::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
