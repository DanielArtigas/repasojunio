<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/exams/new', 'ExamController@new');
Route::post('/exams/new', 'ExamController@newSetModule');
Route::get('/exams/reset', 'ExamController@reset');
Route::get('/exams/save', 'ExamController@save');
Route::get('/exams/questions/{id}', 'ExamController@switchQuestion');


Route::get('/exams/forget', 'ExamController@forget');
Route::resource('/exams', 'ExamController');
Route::get('/exams/{id}/remember', 'ExamController@remember');


Route::resource('/questions', 'QuestionController');
Route::resource('/users', 'UserController');
Route::resource('/modules', 'ModuleController');
Route::resource('/students', 'StudentController');
Route::resource('/families', 'FamilyController');
Route::resource('/studies', 'StudyController');

Route::post('/studies/{id}/modules',  'StudyController@attachModule');
Route::delete('/studies/{id}/modules', 'StudyController@detachModule');

