<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('exams', 'Api\ExamController')->except(['create','edit']);
Route::resource('questions', 'Api\QuestionController')->except(['create','edit']);
Route::resource('users', 'Api\UserController');
Route::resource('modules', 'Api\ModuleController')->except(['create','edit']);
Route::resource('families', 'Api\FamilyController');
Route::resource('studies', 'Api\StudyController');

